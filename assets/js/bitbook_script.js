function geolocate() {
	
	$.ajax({
		url: 'http://freegeoip.net/json/',
		dataType: 'json',
		async: false,
		crossDomain: false,
		success: function(data, textStatus, jqXHR) {
			var html = '';
			$.each(data, function(key, value) {
				html += '<li><b>' + key + '</b>: ' + value + '</li>';
			});
			$('#list').html(html);
		},
		error: function(jqXHR, textStatus, errorThrown) {
			$('#list').html('<li><b>ERROR</b>: '+textStatus+'</li>');
		}
	});

}

var Links = {
	rating_images : new Array(),

	init:function() {
		Links.rating_images[0] = 'assets/images/rating_2_0.gif';
		Links.rating_images[1] = 'assets/images/rating_2_5.gif';
		Links.rating_images[2] = 'assets/images/rating_3_0.gif';
		Links.rating_images[3] = 'assets/images/rating_3_5.gif';
		Links.rating_images[4] = 'assets/images/rating_4_0.gif';
		Links.rating_images[5] = 'assets/images/rating_4_5.gif';
		Links.rating_images[6] = 'assets/images/rating_5_0.gif';
	},

	getLinks: function(index) {
		return Links.rating_images[index];
	}
};

var Search = {
	url: 'https://www.googleapis.com/books/v1/volumes?maxResults=5&q=',
	query: '',

	init: function() {
		Links.init();
		Search.getQuery();
		Search.getResult();
	},

	getQuery: function() {
		Search.query = document.getElementById('q').value;
	},

	formURL: function() {
		url = Search.url;
		Search.query = encodeURI(Search.query);
		url = url + Search.query;
		return url;
	},

	getResult: function() {
		$.ajax({
		url: Search.formURL(),//'https://www.googleapis.com/books/v1/volumes?maxResults=10&q=norwegian%20wood',
		dataType: 'json',
		async: false,
		crossDomain: false,
		success: Search.parseResult
		});
	},

	parseResult: function(result, textStatus, jqXHR) {
		/*var html = '';
		$.each(result, function(key, value) {
			html += '<li><b>' + key + '</b>: ' + value + '</li>';
		});
		$('#list').html(html);*/

		// initialise
		search_html = "<table id='results_table'>";
		row_count = 0;
		flag_isbn = 0;
		desc_html = '';

		// extract data from json response
		for (var i = 0; i < result.items.length; i++)
		{
			flag_isbn = 0;
			authors = '';
			category = '';
			title = result.items[i].volumeInfo.title;
			description = result.items[i].volumeInfo.description;
			pageCount = result.items[i].volumeInfo.pageCount;
			rating = result.items[i].volumeInfo.averageRating;
			year_published = result.items[i].volumeInfo.publishedDate.substr(0, 4);

			rating_link = Search.getRatingImage(rating);

			// publisher
			try {
				publisher = result.items[i].volumeInfo.publisher;
			}
			catch(e) {
				publisher = '';
			}

			// ISBN
			try	{
				isbn13 = result.items[i].volumeInfo.industryIdentifiers[1].identifier;
				isbn13 = Search.formatISBN(isbn13);
			}	
			catch(e) {
				flag_isbn = 1;
			}

			// image link
			try {
				image_link = result.items[i].volumeInfo.imageLinks.thumbnail;
				image_link = Search.removeCurl(image_link);
			}
			catch(e) {
				desc_image = '';
			}

			// author(s) name
			for (var j = 0; j < result.items[i].volumeInfo.authors.length; j++)
			{
				if(j > 0)
					authors += ", ";
				authors += result.items[i].volumeInfo.authors[j];
			};

			// category of book
			try {
				for (var j = 0; j < result.items[i].volumeInfo.categories.length; j++)
				{
					if(j > 0)
						category += ", ";
					category += result.items[i].volumeInfo.categories[j];
				};
			}
			catch(e) {
				category = ''
			}
			

			// housekeeping
			if((!publisher) || (publisher == undefined))
				publisher = '';
			if((!pageCount) || (pageCount == undefined))
				pageCount = '\'x\'';
			if((!category) || (category == undefined))
				category = '';
			if(flag_isbn == 1)
				isbn13 = 'ISBN not available :('
			if((!description) || (description == undefined))
				description = 'Sorry, no description available :(';

			/**********************************************
		    GENERATE SEARCH RESULTS PAGE
			FORMAT: 
		    <tbody onclick=''>
		        <tr>
		        	<td class='bb_item_first' rowspan='5'><img class ='bb_search_item_image' src='IMAGE'></td>
		        	<td class='bb_item_title bb_item_first'>TITLE</td>
		        </tr>
		        <tr><td>AUTHOR</td></tr>
		        <tr><td>CATEGORY</td></tr>
		        <tr><td>YEAR</td></tr>
		        <tr><td>PUBLISHER</td></tr>
		        <tr><td class='bb_separator' colspan='2'></td></tr>
		    </tbody>
            ***********************************************/
            search_html += "<tbody onclick=''>";
            search_html += "<tr><td class='bb_item_first' rowspan='5'><img class ='bb_search_item_image' src='" + image_link + "'></td>";
		    search_html += "<td class='bb_item_title bb_item_first'>" + title + "</td></tr>";
		    search_html += "<tr><td>" + authors + "</td></tr>"
		    search_html += "<tr><td>" + category + "</td></tr>"
		    search_html += "<tr><td>" + year_published + "</td></tr>"
		    search_html += "<tr><td>" + publisher + "</td></tr>"
		    search_html += "<tr><td class='bb_separator' colspan='2'></td></tr>"
		  	search_html += "</tbody>"


			/*************************************************
			* GENERATING DESCRIPTION PAGES
			* FORMAT:
			<!-- page number 0 -->
			<div id='desc_page_0'>
			    <!-- header -->
			    <div class='ui-header'>
			        <div class='refresh-icon inline'><a onclick=''><img alt='back' src='s40-theme/images/refresh_40x40.png'></a></div><div class='ui-title inline'><h2>Information</h2></div>
			    </div>
			    <!-- page structure-->
			    <table class='bb_desc_table'>
			        <tbody>
			        <tr><td class='bb_desc_title' colspan='2'>TITLE</td></tr>
			        <tr><td class='bb_desc_author'colspan='2'>By AUTHOR</td></tr>
			        <tr>
			            <td rowspan='6'><img class='bb_desc_image' src='IMAGE'/></td>
			            <td class='bb_desc_rating'><img src='IMAGE'/></td>
			        </tr>
			        <tr><td class='bb_desc_pages'>2008 | 400 pages</td></tr>
			        <tr><td class='bb_desc_pub'>PUBLISHER</td></tr>
			        <tr><td class='bb_isbn_label'>ISBN 13:</td></tr>
			        <tr><td class='bb_isbn_isbn'>ISBN</td></tr>
			        <tr><td class='bb_desc_button'><div><button>Get Prices</button></div></td></tr>
			        <tr><td class='bb_desc_label' colspan='2'>Description</td></tr>
			        <tr><td class='bb_desc_desc' colspan='2'>DESCRIPTION</td></tr>
			        </tbody>
			    </table>
			</div>
			**************************************************/
			desc_html += "<!-- page number 0 -->";
			desc_html += "<div id='desc_page_" + ++row_count + "'>";
			desc_html += "<!-- header -->";
			desc_html += "<div class='ui-header'>";
			desc_html += "<div class='refresh-icon inline'><a onclick=''><img alt='back' src='s40-theme/images/refresh_40x40.png'></a></div><div class='ui-title inline'><h2>Information</h2></div>";
			desc_html += "</div>";
			
			desc_html += "<!-- page structure-->";
			desc_html += "<table class='bb_desc_table'>";
			desc_html += "<tbody>";
			desc_html += "<tr><td class='bb_desc_title' colspan='2'>" + title + "</td></tr>";
			desc_html += "<tr><td class='bb_desc_author'colspan='2'>By " + authors + "</td></tr>";
			desc_html += "<tr>";
			desc_html += "<td rowspan='6'><img class='bb_desc_image' src='" + image_link + "'/></td>";
			desc_html += "<td class='bb_desc_rating'><img src='" + rating_link + "'/></td>";
			desc_html += "</tr>";
			desc_html += "<tr><td class='bb_desc_pages'>" + year_published + " | " + pageCount + " pages</td></tr>";
			desc_html += "<tr><td class='bb_desc_pub'>" + publisher + "</td></tr>";
			desc_html += "<tr><td class='bb_isbn_label'>ISBN 13:</td></tr>";
			desc_html += "<tr><td class='bb_isbn_isbn'>" + isbn13 + "</td></tr>";
			desc_html += "<tr><td class='bb_desc_button'><div><button>Get Prices</button></div></td></tr>";
			desc_html += "<tr><td class='bb_desc_label' colspan='2'>Description</td></tr>";
			desc_html += "<tr><td class='bb_desc_desc' colspan='2'>" + description + "</td></tr>";
			desc_html += "</tbody>";
			desc_html += "</table></div>";
		};
		// END OF LOOP

		// close tags
		search_html += '</table>';

		// append search results
		$('#search_results').html(search_html);

		// append description pages
		$('#descriptions').html(desc_html)
	},

	removeCurl: function(link) {
		if(link.search('&edge=curl') != -1)
			link = link.replace('&edge=curl', '');
		return link;
	},

	formatISBN: function(num) {
		isbn = num.substr(0, 3) + '-' + num.substr(3, 1) + '-' + num.substr(4, 3) + '-' + num.substr(7, 5) + '-' + num.substr(12, 1);
		return isbn;
	},

	getRatingImage: function(rating) {
		index = 0;
		switch(rating)
		{
			case 2.0:
				index = 0; break;

			case 2.5:
				index = 1; break;

			case 3.0:
				index = 2; break;

			case 3.5:
				index = 3; break;

			case 4.0:
				index = 4; break;

			case 4.5:
				index = 5; break;

			case 5.0:
				index = 6; break;
		}
		return Links.rating_images[index];
	}
};